# PROJECT UTS D02 PBP 2021

## Daftar isi

- [Daftar Isi](#daftar-isi)
- [Anggota Kelompok](#anggota-kelompok)
- [Link Herokuapp](#link-herokuapp)
- [Aplikasi yang Diajukan](#aplikasi-yang-diajukan)
- [Persona Pengguna](#persona-pengguna)

## Anggota Kelompok

1. Arya Bintang Pratama Kumaladjati (2006462960)
2. Judah Ariesaka Magaini (2006463042)
3. Kenneth Jonathan (2006463364)
4. Farah Dita Ashilah (2006463723)
5. Melati Eka Putri (2006464266)
6. David Johan Hasiholan Parhusip (2006482211)
7. Adristi Marsalma Nectarine Amaranthi (2006483113)

## Link Herokuapp
**Link Heroapp kelompok**: https://lindungipeduli.herokuapp.com/

## Aplikasi yang Diajukan
1. **Tema**		: Aplikasi E-Health
2. **Judul**	: Lindungi Peduli
3. **Manfaat**	:
   Sebagai wadah yang menyediakan informasi seputar Covid-19, termasuk informasi terkini, pendaftaran vaksin, apotek daring sebagai penyedia obat-obatan, hingga informasi kesehatan lain yang dikemas dalam bentuk artikel. 
4. **Daftar Modul** :
   - Pengaturan Pengguna
      - Login/Register
      - Profile Settings
      - Admin Page
   - Home Page
      - Landing Page (kondisi Covid-19 saat ini)
      - Halaman forum diskusi
   - Fitur Artikel
      - Halaman artikel
      - Fitur comment dan like
   -  Apotek Online (Fitur Kamus Obat)
      - Tampilan kamus
      - Halaman untuk memasukan data kamus
   - Apotek Online (Fitur Pembeli)
      - Membuat flow pembelian obat
      - Fitur Review pembelian obat
   - Apotek Online (Tambahkan Obat + katalog)
      - Fitur menambahkan obat
      - Tampilan halaman katalog apotek
   - Pendaftaran Vaksin (1 orang)
      - Tampilan List Rumah Sakit (disertai jadwal dan jenis vaksin)
      - Fitur pendaftaran vaksin

## Persona Pengguna
Berikut merupakan persona dari pengguna situs web Lindungi Peduli:
   - Visitor **(V)** : Pengguna yang belum login, memiliki akses yang paling dibatasi
   - Login User **(L)** : Pengguna yang sudah login dan bisa mengakses semua fitur. Namun tidak punya otoritas khusus
   - Article Author **(Aut)**: Pengguna khusus yang berperan sebagai penulis artikel pada situs web
   - Seller **(S)**: Pengguna khusus yang berperan sebagai penyedia obat-obatan yang dijual pada situs web
   - Admin **(Adm)**: Pengguna yang memiliki akses ke semua fitur (baik otoritas khusus atau bukan) dan memiliki wewenang untuk menghapus suatu konten/produk

**Akses setiap persona**:
1. Halaman Login: **Semua bisa mengakses**
2. Halaman Register: **V, Adm**
3. Pengaturan pengguna: **L, Aut, S, Adm**
4. Fitur penambahan obat: **S, Adm**
5. Mengontrol obat yang dijual: **Adm**
6. Melihat kamus dan katalog obat: **V, L, S, Adm**
7. Membeli obat dan review pembelian: **L, Adm**
8. Menulis artikel: **Aut, Adm**
9. Mengontrol artikel terpublikasi: **Adm**
10. Melihat artikel: **V, L, Aut, Adm**
11. Comment dan Likes: **L, Aut, Adm**
12. Melihat halaman jadwal vaksin: **V, L, Adm**
13. Melihat daftar lokasi vaksin: **V, L, Adm**
14. Mendaftar untuk vaksin: **L, Adm**
